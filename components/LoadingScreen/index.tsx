import { FaSpinner } from "react-icons/fa"
import BaseLayout from "../layouts/BaseLayout"
import Logo from "../Logo"

const LoadingScreen = () => {
  return (
    <BaseLayout>
      <div className="bg-white px-10 py-8 flex flex-col gap-3 justify-center items-center">
        <Logo size="large"/>
        <p className="text-xl">
          A moment please
        </p>
        <div className="text-4xl text-blue-900 animate-spin">
          <FaSpinner />
        </div>
      </div>
    </BaseLayout>
  )
}

export default LoadingScreen
