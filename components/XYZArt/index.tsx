import { FunctionComponent, ReactNode, useCallback, useEffect, useState } from "react"
import { ZodErrorResponse } from "../../lib/apiHelper/types"
import { useAuth } from "../../lib/contexts/AuthContext"
import useFormFields from "../../lib/hooks/useFormFields"
import { DirectionsEnum, XYZInput, XYZOutput } from "../../models/xyz"
import Input from "../Input"
import InputLabel from "../InputLabel"

const XYZArt: FunctionComponent<{ children?: ReactNode }> = () => {
  const { renderedFormFields, actualFormFields, formChangeHandler } = useFormFields<XYZInput>({
    letters: 'XYZ',
    size: 3,
    direction: 'horizontal'
  })
  const { accessToken } = useAuth()

  const [xyzOutput, setXyzOutput] = useState<XYZOutput | undefined>()
  const [errors, setErrors] = useState<Partial<Record<keyof XYZInput, string>>>({
  })

  const fetchXYZ = useCallback(async () => {
    try {
      // Create the url
      const url = new URL(window.location.origin + '/api/xyz')

      // Append the search params
      Object.entries(actualFormFields).map(([k, v]) => {
        if (v) {
          url.searchParams.append(k, v.toString())
        }
      })

      // Clear the errors on fetch
      setErrors({})
      const resp = await fetch(url.href, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + accessToken,
        }
      })

      const json = await resp.json()

      switch (resp.status) {
        case 200:
          setXyzOutput(XYZOutput.parse(json))
          break
        case 422:
          Object.keys(XYZInput.keyof().Values).map((k) => {
            ((json as ZodErrorResponse).errors.find((v) => {
              if (v.path.includes(k))
                setErrors(old => {
                  return {
                    ...old,
                    [k]: v.message
                  }
                })
            }))
          })
          break
        case 401:
          break
        default:
          alert('Failed to fetch data')
          break
      }
    } catch (err) {
      alert(err)
    }
  }, [accessToken, actualFormFields])

  useEffect(() => { fetchXYZ() }, [actualFormFields, fetchXYZ])

  return (
    <div className="flex flex-col justify-center items-center gap-3 my-2 w-5/12">
      <form
        onSubmit={(e) => {
          e.preventDefault()
          fetchXYZ()
        }}
      >
        <div className="flex flex-col gap-2">
          <div className="flex flex-col gap-x-2">
            <InputLabel label="Letters" />
            <Input
              className={errors.letters ? 'border-red-500' : ''}
              placeholder="XYZ"
              onChange={formChangeHandler('letters')}
              value={renderedFormFields.letters}
            />
            <p className="text-red-500 text-xs italic" hidden={!!!errors.letters}>{errors.letters}</p>
          </div>
          <div className="flex flex-col gap-x-2">
            <InputLabel label="Size" />
            <Input
              className={errors.size ? 'border-red-500' : ''}
              placeholder="3"
              type="number"
              min="3"
              value={renderedFormFields.size}
              onChange={formChangeHandler('size')}
            />
            <p className="text-red-500 text-xs italic" hidden={!!!errors.size}>{errors.size}</p>
          </div>
          <div className="flex flex-col gap-x-2">
            <InputLabel label="Direction" />
            <select
              className="border rounded py-2 px-3 invalid:border-red-500"
              onChange={formChangeHandler('direction')}
              value={renderedFormFields.direction}
            >
              {Object.keys(DirectionsEnum.Values).map((v) => {
                return <option key={v}>{v}</option>
              })}
            </select>
          </div>
        </div>
        <input hidden type="submit" />
      </form>
      <textarea
        className="flex-1 font-mono min-w-full min-h-[96px] leading-4 whitespace-pre outline-none"
        readOnly
        value={xyzOutput?.xyzArt ?? ""}
      />
    </div>
  )
}

export default XYZArt
