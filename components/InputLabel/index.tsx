import { DetailedHTMLProps, LabelHTMLAttributes } from "react"

interface InputLabelProps extends DetailedHTMLProps<LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement> {
  label: string
}

const InputLabel = ({ className, label, ...rest }: InputLabelProps) => {
  return (
    <label
      className={`text-sm font-bold ${className}`}
      {...rest}
    >
      {label}
    </label>
  )
}

export default InputLabel
