interface LogoSize {
  first: string
  second: string
}

interface LogoSizes {
  small: LogoSize
  large: LogoSize
}

const logoSizes: LogoSizes = {
  small: {
    first: "text-xl",
    second: "text-base"
  },
  large: {
    first: "text-4xl",
    second: "text-2xl"
  }
}

interface LogoProps {
  size: keyof LogoSizes
}

const Logo = ({ size }: LogoProps) => {
  const { first, second } = logoSizes[size]

  return (
    <div>
      <span className={`font-black ${first}`}>
        XYZ
      </span>
      <span className={`font-light font-mono ${second}`}>
        art
      </span>
    </div >
  )
}

export default Logo
