import { useRouter } from "next/router"
import { FunctionComponent, ReactNode } from "react"
import { useAuth } from "../../lib/contexts/AuthContext"
import LoadingScreen from "../LoadingScreen"


const ProtectedRoute: FunctionComponent<{ children?: ReactNode }> = ({
  children
}) => {
  const router = useRouter()
  const { isAuthenticated, isLoading } = useAuth()

  if (isLoading) {
    return <LoadingScreen />
  }

  if (!isLoading && !isAuthenticated) {
    router.push('/signin')
    return <LoadingScreen />
  } else {
    return <>{children}</>
  }
}

export default ProtectedRoute
