import { DetailedHTMLProps, InputHTMLAttributes } from "react"

type InputProps = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>

const Input = ({ className, ...rest }: InputProps) => {
  return (
    <input
      className={`border rounded py-2 px-3 focus:outline-none focus:bg-gray-100 transition-colors ${className}`}
      {...rest}
    />
  )
}

export default Input
