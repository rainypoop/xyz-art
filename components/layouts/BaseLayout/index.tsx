import { FunctionComponent, ReactNode } from 'react'

const BaseLayout: FunctionComponent<{ children?: ReactNode }> = ({ children }) => {
  return (
    <div className='flex flex-col min-h-screen justify-between bg-blue-50'>
      <main className='flex flex-1 justify-center items-center'>
        {children}
      </main>
    </div>
  )
}

export default BaseLayout
