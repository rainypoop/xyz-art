import { FunctionComponent, ReactNode } from 'react'
import Footer from '../../Footer'
import Navbar from '../../Navbar'
import ProtectedRoute from '../../ProtectedRoute'

const DashboardLayout: FunctionComponent<{ children?: ReactNode }> = ({ children }) => {
  return (
    <ProtectedRoute>
      <div className='flex flex-col min-h-screen justify-between'>
        <Navbar />
        <main className='flex flex-1 justify-center bg-blue-50'>
          {children}
        </main>
        <Footer />
      </div>
    </ProtectedRoute>
  )
}

export default DashboardLayout
