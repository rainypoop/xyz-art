import Link from "next/link"
import { FaUserCircle } from "react-icons/fa"
import { useAuth } from "../../lib/contexts/AuthContext"
import Logo from "../Logo"

const Navbar = () => {
  const { user, signOut } = useAuth()

  return (
    <header className="flex flex-wrap items-center justify-between p-4 gap-4 shadow-sm">

      <Link href="#">
        <a>
          <Logo size="small" />
        </a>
      </Link>

      <div className="relative group">
        <button className="">
          <FaUserCircle size="1.5rem" />
        </button>
        <div className="absolute flex-col items-center gap-3 right-0 z-50 p-3 bg-white shadow-md whitespace-nowrap hidden group-focus-within:flex group-focus-within:animate-fade-in">
          <div>
            <h2 className="font-medium">{user?.username}</h2>
          </div>
          <div>
            <button
              className="border rounded-sm py-1 px-4"
              onClick={() => signOut()}
            >
              Sign out
            </button>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Navbar
