import { User } from "../models/users";

// PROOF OF CONCEPT ONLY
const USERS: User[] = [
  { id: 1, username: "username", password: "password" }
]

/**
 * Gets the user from the database by username.
 * @param username 
 * @returns {User} Returns a user object if found, undefined otherwise.
 */
export async function getUser(username: string): Promise<User | undefined> {
  const user = USERS.find((user) => {
    return user.username == username
  })
  return user
}
