import { sign, verify } from "jsonwebtoken"
import { UnauthorizedError } from "../lib/apiHelper/errors"
import type { User } from "../models/users"
import { UserDto } from "../models/users"
import { getUser } from "./users"

/**
 * Creates a JWT access token representing the user object.
 * @returns {string} Returns a JWT access token
 */
export async function verifyUser(username: string, password: string): Promise<string> {
  const user = await getUser(username)

  if (!user || user.password != password) throw new UnauthorizedError("Incorrect username or password.")

  return signJwt(user)
}

export function signJwt(user: User): string {
  const userDto = UserDto.parse(user)
  return sign(userDto, process.env.API_KEY ?? "")
}

export function verifyJwt(token: string) {
  return verify(token, process.env.API_KEY ?? "")
}
