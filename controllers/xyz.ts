import type { AllowedLetters, DirectionsEnum } from "../models/xyz"

const TEXT_FILL = "O"

export function generateXyzLetterArt(letter: AllowedLetters, size: number): string[][] {
  const a: string[][] = new Array(size).fill([]).map(() => {
    return new Array(size).fill(" ")
  })
  const midIndex = Math.floor(size / 2)

  if (letter == "X") {
    for (let i = 0; i < size; i++) {
      a[i][i] = TEXT_FILL
      a[i][size - 1 - i] = TEXT_FILL
    }
  }
  else if (letter == "Y") {
    for (let i = 0; i < size; i++) {
      if (i < midIndex) {
        a[i][i] = TEXT_FILL
        a[i][size - i - 1] = TEXT_FILL
      } else {
        a[i][midIndex] = TEXT_FILL
      }
    }
  }
  else if (letter == "Z") {
    for (let i = 0; i < size; i++) {
      if (i == 0 || i == size - 1) {
        a[i] = new Array(size).fill(TEXT_FILL)
      } else {
        a[i][size - 1 - i] = TEXT_FILL
      }
    }
  }
  return a
}

export function generateXyzArt(letters: string, size: number, direction: DirectionsEnum): string {
  const xyzArt = Array.from(letters)
    .map((l) => {
      return generateXyzLetterArt(l as AllowedLetters, size)
    })
    .reduce((prevChar, currChar) => {
      if (direction == "vertical") {
        // Append the next character to the bottom separated by space
        prevChar.push(new Array(size).fill(" "))
        prevChar.push(...currChar)
        return prevChar
      } else {
        // Append the next character to the right separated by space
        return prevChar.map((charRow, i) => {
          return [...charRow, " ", ...currChar[i]]
        })
      }
    })
    .reduce((prev, curr, i) => {
      return prev + (i == 0 ? "" : "\n") + curr.join("")
    }, "")
  return xyzArt
}
