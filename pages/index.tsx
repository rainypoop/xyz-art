import type { NextPage } from 'next'
import DashboardLayout from '../components/layouts/DashboardLayout'
import XYZArt from '../components/XYZArt'

const Home: NextPage = () => {
  return (
    <DashboardLayout>
      <XYZArt />
    </DashboardLayout>

  )
}

export default Home
