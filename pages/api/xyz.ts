import type { NextApiRequest } from 'next'
import { generateXyzArt } from '../../controllers/xyz'
import { apiHandler } from '../../lib/apiHelper/apiHandler'
import { NextApiWithErrorResponse } from '../../lib/apiHelper/types'
import type { XYZOutput } from '../../models/xyz'
import { XYZInput } from '../../models/xyz'

async function xyzHandler(
  req: NextApiRequest,
  res: NextApiWithErrorResponse<XYZOutput>
) {
  const { letters, size, direction } = XYZInput.parse(req.query)
  const xyzArt = generateXyzArt(letters, size, direction)
  console.log(xyzArt)

  res.status(200).json({
    letters,
    size,
    direction,
    xyzArt
  })
}

export default apiHandler({
  methods: { get: xyzHandler },
  requireAccessToken: true
})
