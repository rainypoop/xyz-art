import { NextApiRequest } from "next"
import { NextApiWithErrorResponse } from "../../../lib/apiHelper/types"
import { SignInInput } from "../../../models/auth"
import type { SignInOutput } from "../../../models/auth"
import { apiHandler } from "../../../lib/apiHelper/apiHandler"
import { verifyUser } from "../../../controllers/auth"

async function signIn(
  req: NextApiRequest,
  res: NextApiWithErrorResponse<SignInOutput>
) {
  const { username, password } = SignInInput.parse(req.body)
  const token = await verifyUser(username, password)
  return res.status(200).send({ accessToken: token })
}

export default apiHandler({
  methods: { post: signIn },
  requireAccessToken: false
})
