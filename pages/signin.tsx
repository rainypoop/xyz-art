import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import Input from '../components/Input'
import InputLabel from '../components/InputLabel'
import BaseLayout from '../components/layouts/BaseLayout'
import LoadingScreen from '../components/LoadingScreen'
import Logo from '../components/Logo'
import { useAuth } from '../lib/contexts/AuthContext'
import useFormFields from '../lib/hooks/useFormFields'
import type { SignInInput } from "../models/auth"

const SignIn: NextPage = () => {
  const { signIn, isError, isLoading, isAuthenticated } = useAuth()
  const router = useRouter()
  const { actualFormFields, formChangeHandler } = useFormFields<SignInInput>({ username: '', password: '' }, 0)

  if (isAuthenticated) {
    router.push('/')
  }

  if (isLoading) {
    return <LoadingScreen />
  }

  return (
    <BaseLayout>
      <div className='flex flex-col p-8 gap-5 justify-center items-center w-full mx-20 h-screen sm:w-auto sm:h-auto bg-white shadow-md'>
        <Logo size='large' />
        <h2 className='text-center text-lg'>Sign in to your account</h2>
        <form
          className='flex flex-col gap-3 w-full'
          onSubmit={(e) => {
            e.preventDefault()
            signIn(actualFormFields)
          }}
        >
          <div className='flex flex-col'>
            <InputLabel label='Username' />
            <Input
              className={isError ? 'border-red-500 ' : ''}
              type="text"
              placeholder="Username"
              onChange={formChangeHandler('username')}
            />
          </div>
          <div className='flex flex-col'>
            <InputLabel label='Password' />
            <Input
              className={isError ? 'border-red-500 ' : ''}
              type="password"
              placeholder="*********"
              onChange={formChangeHandler('password')}
            />
          </div>
          <button className='rounded border my-3 py-1 bg-blue-500 text-gray-200 hover:brightness-125 transition-all'>Sign In</button>
        </form>
        <p className={`text-red-500 text-xs italic ${isError ? '' : 'hidden'}`}>Incorrect username/password.</p>
      </div>
    </BaseLayout>
  )
}

export default SignIn
