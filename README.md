# Information

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## API Endpoints

### api/xyz

_\*requires access token_

This endpoint has three optional query parameters:

- `letters`: A string of letters containing only x, y, z (case-insensitive). Defaults to `XYZ`.
- `size`: An odd positive integer referring to the block size of each letter. Defaults to `3`.
- `direction`: Can either be `horizontal` or `vertical`. defaults to `horizontal`.

Returns the following:

```json
{
  "letters": "string",
  "size": "number",
  "direction": "string",
  "xyzArt": "string"
}
```

### api/signin

This endpoint provides an access token for the `api/xyz` endpoint.

Requires the following request body:

```json
{
  "username": "string",
  "password": "string"
}
```

Returns the following:

```json
{
  "accessToken": "string"
}
```

# Setup

This project is build and tested on Node v16.16.0

- `Node 16.16.0`
- [Classic Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable)
- Run `yarn install` to install the project's dependencies

## Environment variables

Copy and rename the file named `.env.local.example` to `.env.local` using the following command:

```
cp .env.local.example .env.local
```

Modify the `.env.local` to edit the local environment variables.

## Starting the application

Run the program by executing the following commands:

```
yarn build
yarn start
```
