import { z } from "zod"
import { AllowedLetters, XYZInput } from "./xyz"

describe('Allowed Letters',()=>{
  const invalidLetters = "sasajkdlsa"
  const validLetters = "xyxzxyXZY"
  
  const validLowercase = "xyzxyz"
  const validUppercase = "XYZXYZ"
  const validMixedcase = "xYzXYz"

  it("does not parse invalid letters", ()=>{
    const parseInvalid = ()=> AllowedLetters.parse(invalidLetters)
    expect(parseInvalid).toThrow(z.ZodError)
  })

  it("parses valid letters", ()=>{
    const parsed = AllowedLetters.parse(validLetters)
    expect(parsed).toBeDefined()
  })

  it("capitalizes after parsing", ()=>{
    const parsedLower = AllowedLetters.parse(validLowercase)
    const parsedUpper = AllowedLetters.parse(validUppercase)
    const parsedMixed = AllowedLetters.parse(validMixedcase)

    expect(parsedLower).toBeDefined()
    expect(parsedUpper).toBeDefined()
    expect(parsedMixed).toBeDefined()

    expect(parsedLower).toEqual(parsedUpper)
    expect(parsedUpper).toEqual(parsedMixed)
  })
})

describe("XYZInput Model", ()=>{
  const invalidLetters = "sasajkdlsa"
  const validLetters = "xyzxyz"

  const invalidNonNumericStringSize = "fjkda"
  const invalidNegativeStringSize = "-5"
  const invalidZeroSize = "0"
  const invalidFractionalSize = "0.5"
  const invalidBelowThreeSize = "2"
  const invalidEvenAboveThreeSize = "4"
  const validThreeSize = "3"
  const validAboveThreeSize = "5"

  const validHorizontalDirection = "horizontal"
  const validVerticalDirection = "vertical"
  const invalidOtherDirection = "others"

  // TODO: Write 2*8*3=48 test cases
  it("parses valid letters, valid size, valid direction",()=>{
    XYZInput.parse({
      letters: validLetters,
      size: validThreeSize,
      direction: validVerticalDirection
    })

    XYZInput.parse({
      letters: validLetters,
      size: validAboveThreeSize,
      direction: validVerticalDirection
    })
  }
  )
})
