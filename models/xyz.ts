import { z } from "zod";

export const DirectionsEnum = z.enum(["horizontal", "vertical"])
export const AllowedLetters = z.string().regex(/^[xyzXYZ]*$/, "Only XYZ are allowed").transform(v => v.toUpperCase())

export type AllowedLetters = z.infer<typeof AllowedLetters>
export type DirectionsEnum = z.infer<typeof DirectionsEnum>

// define input and output models
export const XYZInput = z.object({
  letters: AllowedLetters
    .default("XYZ"),
  size: z.number()
    .or(z.string())
    .superRefine((v, ctx) => {
      if (!/\d+/.test(v.toString())) {
        ctx.addIssue(
          {
            message: "Only integers are allowed",
            fatal: true,
            code: "custom"
          }
        )
      }
    }).transform(Number)
    .refine(n => Number.isInteger(n), "Only integers are allowed")
    .refine(n => n >= 3, "Must be greater than or equal to 3")
    .refine(n => n % 2 != 0, "Must be odd")
    .default(3),
  direction: DirectionsEnum
    .default("horizontal")
});

export const XYZOutput = XYZInput.extend({
  xyzArt: z.string()
})

// extract the inferred type
export type XYZInput = z.infer<typeof XYZInput>
export type XYZOutput = z.infer<typeof XYZOutput>
