import { z } from "zod"

export const User = z.object({
  id: z.number().int(),
  username: z.string().min(6),
  password: z.string().min(6)
})
export const UserDto = User.omit({ password: true })

export type User = z.infer<typeof User>
export type UserDto = z.infer<typeof UserDto>
