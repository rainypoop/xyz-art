import { z } from "zod"
import { User } from "./users"

export const SignInInput = User.omit({ id: true })

export const SignInOutput = z.object({
  accessToken: z.string()
})

export type SignInInput = z.infer<typeof SignInInput>
export type SignInOutput = z.infer<typeof SignInOutput>
