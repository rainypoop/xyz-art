import { NextApiRequest, NextApiResponse } from "next"
import { z } from "zod"

export interface ErrorResponse {
    message: string
}
export interface ZodErrorResponse extends ErrorResponse {
    errors: z.ZodIssue[]
}
export type NextApiWithErrorResponse<T> = NextApiResponse<T | ErrorResponse | ZodErrorResponse>
export type AllowedMethods = "post" | "get"
export type HandlerFunction<T> = (req: NextApiRequest, res: NextApiWithErrorResponse<T>) => Promise<void>
export type MethodHandler<T> = {
    methods: Partial<Record<AllowedMethods, HandlerFunction<T>>>
    requireAccessToken: boolean
}
