import { JsonWebTokenError } from "jsonwebtoken"
import { NextApiRequest } from "next"
import { z } from "zod"
import { verifyJwt } from "../../controllers/auth"
import { UserDto } from "../../models/users"
import { APIError, UnauthorizedError } from "./errors"
import { AllowedMethods, MethodHandler, NextApiWithErrorResponse } from "./types"

export function apiHandler<T = Record<string, unknown>>(handler: MethodHandler<T>) {
  return async (req: NextApiRequest, res: NextApiWithErrorResponse<T>) => {
    const method = (req.method?.toLowerCase()) as AllowedMethods
    const { requireAccessToken, methods } = handler
    const methodHandler = methods[method]

    // Only return JSON type for now
    res.setHeader("Content-Type", "application/json")

    // Check the method handler
    if (!methodHandler) {
      return res.status(405).send({ message: `Method ${req.method} Not Allowed` })
    }

    try {
      // Check token
      if (requireAccessToken) {
        const token = req.headers.authorization
        if (!token) throw new UnauthorizedError

        const split = token.split(" ")
        if (split[0] != "Bearer") throw new UnauthorizedError("Invalid authorization scheme.")

        UserDto.parse(verifyJwt(split[1]))
      }

      await methodHandler(req, res)
    } catch (e) {
      // Error handling
      if (e instanceof z.ZodError) {
        res.status(422).send({ errors: e.issues, message: e.name })
      }
      else if (e instanceof APIError) {
        res.status(e.code).send({ message: e.message })
      } else if (e instanceof JsonWebTokenError) {
        res.status(401).send({ message: "Invalid access token." })
      }
      else {
        res.status(500).send({ message: e as string })
      }
    }
  }
}
