export class APIError extends Error{
  code: number
  message: string

  constructor(code: number, message: string){
    super()
    this.code = code
    this.message=message
  }
}

export class NotFoundError extends APIError{
  constructor(message = "Resource not found"){
    super(404, message)
  }
}

export class UnauthorizedError extends APIError{
  constructor(message = "Unauthorized"){
    super(401, message)
  }
}
