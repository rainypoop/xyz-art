import { decode } from "jsonwebtoken"
import { useRouter } from "next/router"
import { createContext, FunctionComponent, ReactNode, useContext, useEffect, useState } from "react"
import { SignInInput, SignInOutput } from "../../models/auth"
import { UserDto } from "../../models/users"

type SignIn = (signInData: SignInInput) => Promise<void>
type SignOut = (callbackUrl?: string) => void

export interface AuthState {
  user: UserDto | null
  accessToken: string | null
  signIn: SignIn
  signOut: SignOut
  isLoading: boolean
  isError: boolean
  isAuthenticated: boolean
}

const AuthContext = createContext<AuthState | undefined>(undefined)

export const AuthProvider: FunctionComponent<{ children?: ReactNode }> = ({ children }) => {
  const router = useRouter()
  const [user, setUser] = useState<UserDto | null>(null)
  const [accessToken, setAccessToken] = useState<string | null>(null)
  const [isLoading, setLoading] = useState(true)
  const [isError, setError] = useState(false)
  const isAuthenticated = !!user

  const signIn: SignIn = async (signInData: SignInInput) => {
    setLoading(true)
    try {
      const resp = await fetch(window.location.origin + '/api/auth/signin', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(signInData)
      })

      const { accessToken: respAccessToken } = SignInOutput.parse(await resp.json())
      setAccessToken(respAccessToken)
      setError(false)
      localStorage.setItem("accessToken", respAccessToken)
    } catch (err) {
      setAccessToken(null)
      setError(true)
      setLoading(false)
      localStorage.removeItem("accessToken")
    }
  }

  const signOut: SignOut = (callbackUrl?: string) => {
    setLoading(false)
    setAccessToken(null)
    localStorage.removeItem("accessToken")
    router.push(callbackUrl || "/signin")
  }

  // Get accessToken from localStorage and parse user
  useEffect(() => {
    const storedToken = localStorage.getItem("accessToken")

    setLoading(true)
    setAccessToken(storedToken)

    if (storedToken) {
      let parsedUser
      try {
        parsedUser = UserDto.parse(decode(storedToken))
      } catch (err) {
        parsedUser = null
      }
      setUser(parsedUser)
    } else {
      setUser(null)
    }
    setLoading(false)
  }, [accessToken])

  return (
    <AuthContext.Provider
      value={{
        user,
        accessToken,
        signIn,
        signOut,
        isAuthenticated,
        isError,
        isLoading,
      }}
    >{children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => {
  const context = useContext(AuthContext)

  if (context == undefined) {
    throw new Error('Must be used within AuthProvider')
  }

  return context
}
