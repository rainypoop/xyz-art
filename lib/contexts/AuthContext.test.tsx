import { act, renderHook } from '@testing-library/react'
import { AuthProvider, AuthState, useAuth } from './AuthContext'

describe('AuthContext', () => {
  beforeEach(() => {
    localStorage.clear()
  })

  describe('State', () => {
    it('should be initialized as unauthenticated', () => {
      const { result } = renderHook(() => useAuth(), { wrapper: AuthProvider })

      expect(result.current.user).toBeNull()
      expect(result.current.accessToken).toBeNull()
      expect(result.current.isLoading).toBeFalsy()
      expect(result.current.isAuthenticated).toBeFalsy()
    })
  })

  describe('signIn', () => {
    it('should authenticate user with correct credentials', async () => {
      global.fetch = jest.fn().mockImplementationOnce(() =>
        Promise.resolve({
          json: () => Promise.resolve({ accessToken: authFixture().accessToken }),
        } as Response),
      )
      const { user, accessToken } = authFixture()
      const { result } = renderHook(() => useAuth(), { wrapper: AuthProvider })

      expect(result.current.isLoading).toBeFalsy()
      expect(result.current.isAuthenticated).toBeFalsy()

      await act(async () => {
        await result.current.signIn({ username: "username", password: "password" })
      })

      expect(result.current.accessToken).toStrictEqual(accessToken)
      expect(result.current.user).toStrictEqual(user)
      expect(result.current.isAuthenticated).toBeTruthy()
      expect(result.current.isLoading).toBeFalsy()
    })

    it('should not authenticate user with incorrect credentials', async () => {
      global.fetch = jest.fn().mockImplementationOnce(() => Promise.reject())

      const { result } = renderHook(() => useAuth(), { wrapper: AuthProvider })

      expect(result.current.isAuthenticated).toBeFalsy()

      await act(async () => {
        result.current.signIn({ username: "wrong", password: "wrong" })
      })

      expect(result.current.accessToken).toBeNull()
      expect(result.current.user).toBeNull()
      expect(result.current.isAuthenticated).toBeFalsy()
    })
  })

  describe('signOut', () => {
    it('should unauthenticate the user', () => {
      // taken from: https://github.com/vercel/next.js/issues/7479
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const useRouter = jest.spyOn(require('next/router'), 'useRouter')
      const router = { push: jest.fn() }
      useRouter.mockReturnValue(router)

      const { result } = renderHook(() => useAuth(), { wrapper: AuthProvider })

      act(() => {
        result.current.signOut()
      })

      expect(result.current.accessToken).toBeNull()
      expect(result.current.user).toBeNull()
      expect(result.current.isAuthenticated).toBeFalsy()
    })
  })
})

const authFixture = (
  credentials: Partial<AuthState> = {},
): Pick<AuthState, "user" | "accessToken"> => {
  return {
    user: { "id": 1, "username": "username" },
    accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ1c2VybmFtZSJ9.HFv7ATkpURueVbI79JGRpSRuhf30B8G4kBtY-BM8K-Y",
    ...credentials,
  }
}
