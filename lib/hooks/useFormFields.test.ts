import { act, renderHook } from '@testing-library/react'
import { ChangeEvent } from 'react'
import useFormFields from './useFormFields'

describe('useFormFields', () => {
  jest.useFakeTimers()
  const initialState = {
    username: '',
    password: '',
  }
  describe('formFields', () => {
    it('should initialize to the given initialState', () => {
      const { result } = renderHook(() => useFormFields(initialState))

      expect(result.current.actualFormFields).toStrictEqual({
        username: '',
        password: '',
      })

      expect(result.current.renderedFormFields).toStrictEqual({
        username: '',
        password: '',
      })
    })
  })

  describe('formChangeHandler', () => {
    it('should change the state of the renderedFormFields without delay', () => {
      const { result } = renderHook(() => useFormFields(initialState))
      const { formChangeHandler } = result.current

      let mockEvent = {
        target: {
          value: 'new username',
        },
      } as ChangeEvent<HTMLInputElement>

      act(() => formChangeHandler('username')(mockEvent))

      mockEvent = {
        target: {
          value: 'new password',
        },
      } as ChangeEvent<HTMLInputElement>

      act(() => formChangeHandler('password')(mockEvent))

      expect(result.current.renderedFormFields).toStrictEqual({
        username: 'new username',
        password: 'new password',
      })
    })

    it('should change the state of the actualFormFields after delay', () => {
      const { result } = renderHook(() => useFormFields(initialState))
      const { formChangeHandler } = result.current

      let mockEvent = {
        target: {
          value: 'new username',
        },
      } as ChangeEvent<HTMLInputElement>

      act(() => formChangeHandler('username')(mockEvent))

      mockEvent = {
        target: {
          value: 'new password',
        },
      } as ChangeEvent<HTMLInputElement>

      act(() => formChangeHandler('password')(mockEvent))

      expect(result.current.actualFormFields).not.toStrictEqual({
        username: 'new username',
        password: 'new password',
      })

      act(() => {jest.runAllTimers()})

      expect(result.current.actualFormFields).toStrictEqual({
        username: 'new username',
        password: 'new password',
      })
    })


    describe('with `int` as dataType', () => {
      it('should convert a string integer to an actual integer', () => {
        const { result } = renderHook(() => useFormFields({ num: 5 }))
        const { formChangeHandler } = result.current

        const mockEvent = {
          target: {
            value: '123',
          },
        } as ChangeEvent<HTMLInputElement>

        act(() => formChangeHandler('num', 'int')(mockEvent))

        expect(result.current.renderedFormFields).toStrictEqual({
          num: 123,
        })

        act(() => { jest.runAllTimers() })

        expect(result.current.actualFormFields).toStrictEqual({
          num: 123,
        })
      })

      it('should convert an invalid string integer to zero', () => {
        const { result } = renderHook(() => useFormFields({ num: 5 }))
        const { formChangeHandler } = result.current

        const mockEvent = {
          target: {
            value: 'notavalidint',
          },
        } as ChangeEvent<HTMLInputElement>

        act(() => formChangeHandler('num', 'int')(mockEvent))

        expect(result.current.renderedFormFields).toStrictEqual({
          num: 0,
        })

        act(() => { jest.runAllTimers() })

        expect(result.current.actualFormFields).toStrictEqual({
          num: 0,
        })

      })
    })
  })
})
