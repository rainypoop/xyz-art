import { ChangeEvent, Dispatch, SetStateAction, useEffect, useState } from 'react'

// add more data types as they are needed
export type dataType = 'int'

export type Event =
  | ChangeEvent<HTMLInputElement>
  | ChangeEvent<HTMLSelectElement>
export interface IUseFormFields<T> {
  actualFormFields: T
  renderedFormFields: T
  setRenderedFormFields: Dispatch<SetStateAction<T>>
  formChangeHandler: (key: keyof T, dataType?: dataType) => (e: Event) => void
}

const resolveDataType = (value: string, dataType?: dataType) => {
  if (dataType === 'int') {
    const x = parseInt(value)

    if (isNaN(x)) {
      return 0
    }

    return x
  }

  return value
}

const useFormFields = <T>(initialState: T, delay = 500): IUseFormFields<T> => {
  const [actualFormFields, setActualFormFields] = useState<T>(initialState)
  const [renderedFormFields, setRenderedFormFields] = useState<T>(initialState)

  const formChangeHandler =
    (key: keyof T, dataType?: dataType) => (e: Event) => {
      const value = e.target.value

      const finalValue = resolveDataType(value, dataType)

      setRenderedFormFields((curr) => {
        return {
          ...curr,
          [key]: finalValue,
        }
      })
    }

  useEffect(() => {
    const timeOutId = setTimeout(() => setActualFormFields(renderedFormFields), delay)
    return () => clearTimeout(timeOutId)
  }, [delay, renderedFormFields])

  return {
    actualFormFields,
    renderedFormFields,
    setRenderedFormFields,
    formChangeHandler,
  }
}

export default useFormFields
